import pyttsx3
import speech_recognition as sr
from gtts import gTTS
import os
import wikipedia
import webbrowser
import playsound
import requests
from bs4 import BeautifulSoup
import urllib.request
import re
from word2number import w2n
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait

robot_mouth = pyttsx3.init()
robot_ear = sr.Recognizer()
robot_brain = ""
driver = webdriver.Chrome(executable_path='D:/chromedriver_win32/chromedriver.exe')
driver.maximize_window()
wait = WebDriverWait(driver, 3)

def wiki(keyword):
	try:
		robot_brain = "Đang tìm kiếm trên wiki"
		output = gTTS(robot_brain, lang = "vi", slow = False)
		output.save("output.mp3")
		playsound.playsound('output.mp3')
		os.remove('output.mp3')
	except:
		robot_brain = "Xin lỗi, hiện tại tôi không mở được wiki"
		output = gTTS(robot_brain, lang = "vi", slow = False)
		output.save("output.mp3")
		playsound.playsound('output.mp3')
		os.remove('output.mp3')
	run = False
def findYT(search):
	# link_search = "https://www.youtube.com/results?search_query=" + search
	# html = urllib.request.urlopen("https://www.youtube.com/results?search_query=" + search)
	# webbrowser.open_new(link_search)
	# video_ids = re.findall(r"watch\?v=(\S{11})", html.read().decode())
	driver.get('https://www.youtube.com/results?search_query=' + search)
	video_ids = driver.find_elements_by_id('video-title')
	openIndexVideo(video_ids)
	run = False
def openIndexVideo(videos):
	number = 0
	j=0
	robot_brain = "Bạn muốn xem video số mấy?"
	output = gTTS(robot_brain, lang = "vi", slow = False)
	output.save("output.mp3")
	playsound.playsound('output.mp3')
	os.remove('output.mp3')
	while j<3:
		y = sr.Recognizer()
		with sr.Microphone() as wordNumber:
			try:
				# audio = r.listen(wordNumber, timeout = 5, phrase_time_limit=3)
				# stringNumber = y.recognize_google(audio, language="vi-VI")
				number = w2n.word_to_num("twenty-one")
			except:
				j= j+1
				robot_brain = "Bạn muốn xem video số mấy?"
				output = gTTS(robot_brain, lang = "vi", slow = False)
				output.save("output.mp3")
				playsound.playsound('output.mp3')
				os.remove('output.mp3')
				if j==3:
					break
			try:
				videos[number].click()
			except:
				robot_brain = "Mời bạn chọn video khác, chỉ có sẵn video từ 0 đến " + str(len(videos))
				output = gTTS(robot_brain, lang = "vi", slow = False)
				output.save("output.mp3")
				playsound.playsound('output.mp3')
				os.remove('output.mp3')
				j= j+1
			j=3
	run = False
i = 0
while  (i < 3 and True):
	r = sr.Recognizer()
	with sr.Microphone() as source:
		if i == 0:
			robot_brain = "Mời bạn nói"
			output = gTTS(robot_brain, lang = "vi", slow = False)
			output.save("output.mp3")
			playsound.playsound('output.mp3')
			os.remove('output.mp3')
		else:
			robot_brain = ""
		try:
			# audio = r.listen(source)
			# you = r.recognize_google(audio, language = "vi-VI")
			you = "Youtube"
		except:
			i=i+1
			robot_brain = "Tôi không nghe thấy bạn nói gì, bạn có thể nói lại không?"
			output = gTTS(robot_brain, lang = "vi", slow = False)
			output.save("output.mp3")
			playsound.playsound('output.mp3')
			os.remove('output.mp3')
			continue
		if "Chrome" in you:
			webbrowser.open('https://www.google.com.vn', new = 1)
			robot_brain = "Ok, Google đã được mở"
		elif "Youtube" in you:
			driver.get('https://www.youtube.com')
			robot_brain = "Ok, Youtube đã được mở"
			output = gTTS(robot_brain, lang = "vi", slow = False)
			output.save("output.mp3")
			playsound.playsound('output.mp3')
			os.remove('output.mp3')
			yt = ""
			j=0
			while (yt != "" or j<3):
				y = sr.Recognizer()
				with sr.Microphone() as keyword:
					try:
						# audio = r.listen(keyword, timeout = 5, phrase_time_limit=3)
						# yt = y.recognize_google(audio, language="vi-VI")
						yt = "lê bảo bình"
					except:
						j= j+1
						robot_brain = "Bạn muốn tìm gì trên youtube?"
						output = gTTS(robot_brain, lang = "vi", slow = False)
						output.save("output.mp3")
						playsound.playsound('output.mp3')
						os.remove('output.mp3')
						if j==3:
							break
					j=3
					i=3
					if yt != "":
						findYT(yt)
						break
					else:
						continue
		elif "music" in you:
			webbrowser.open('https://www.youtube.com', new = 1)
			robot_brain = "Ok, youtube đang được mở và tìm kiếm nhạc"
		elif "tắt máy" == you:
			robot_brain = "Bạn chắc chắn muốn tắt máy chứ"
			output = gTTS(robot_brain, lang = "vi", slow = False)
			output.save("output.mp3")
			playsound.playsound('output.mp3')
			os.remove('output.mp3')
			shutdown = "no"
			j=0
			while j<3:
				y = sr.Recognizer()
				with sr.Microphone() as keyword:
					try:
						audio = r.listen(keyword, timeout = 5, phrase_time_limit=3)
						shutdown = y.recognize_google(audio, language="vi-VI")
					except:
						j= j+1
						robot_brain = "Bạn có muốn tắt máy nữa không?"
						output = gTTS(robot_brain, lang = "vi", slow = False)
						output.save("output.mp3")
						playsound.playsound('output.mp3')
						os.remove('output.mp3')
						if j==3:
							break
					j=3
					if shutdown == "yes":
						os.system('shutdown -s')
					else:
						continue
		elif "khởi động lại" == you:
			robot_brain = "Bạn chắc chắn muốn khởi động lại máy chứ"
			output = gTTS(robot_brain, lang = "vi", slow = False)
			output.save("output.mp3")
			playsound.playsound('output.mp3')
			os.remove('output.mp3')
			shutdown = "no"
			j=0
			while j<3:
				y = sr.Recognizer()
				with sr.Microphone() as keyword:
					try:
						audio = r.listen(keyword, timeout = 5, phrase_time_limit=3)
						shutdown = y.recognize_google(audio, language="vi-VI")
					except:
						j= j+1
						robot_brain = "Bạn có muốn khởi động lại máy nữa không?"
						output = gTTS(robot_brain, lang = "vi", slow = False)
						output.save("output.mp3")
						playsound.playsound('output.mp3')
						os.remove('output.mp3')
						if j==3:
							break
					j=3
					if shutdown == "yes":
						os.system('shutdown -s')
					else:
						continue
		elif "bye" in you:
			robot_brain = "Ok, bye. See you again!"
			output = gTTS(robot_brain, lang = "vi", slow = False)
			output.save("output.mp3")
			playsound.playsound('output.mp3')
			os.remove('output.mp3')
			break
		elif you == "":
			continue
		else:
			robot_brain = "Xin lỗi, phần này tôi chưa được học!"

		output = gTTS(robot_brain, lang = "vi", slow = False)
		output.save("output.mp3")
		playsound.playsound('output.mp3')
		os.remove('output.mp3')

